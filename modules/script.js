// "use strict";
import { getKey as apiKey } from "./key.js"
// Global Const
const CATEGORIES = {
  "indigenous": "Canadian indigenous history",
  "colonial": "Canadian colonial history"
}
// Number Of Results needed for the api
const MAX_RESULTS = 10

// Event for DOMCONTENTLoaded
document.addEventListener("DOMContentLoaded", init);

/**
 * IFFEE Adds smooth scrolling to navbar links
 */
(() => {
  document.querySelectorAll("#navbar a").forEach(link => {
    link.addEventListener("click", (e) => {
      e.preventDefault()
      const href = link.getAttribute("href")
      document.querySelector(href).scrollIntoView({ behavior: "smooth" })
    })
  })
})()

/**
 * This function gets called when DOMCONTENTLoaded
 */
function init() {
  fetchBooks(CATEGORIES.indigenous)
  fetchBooks(CATEGORIES.colonial)
}

/**
 * Creates an IntersectionObserver for Loading Images Lazely
 * @returns {IntersectionObserver} : imageObserver
 */
function getObserverLazyImages() {
  // const colonialImages  =  document.querySelector('#colonial').querySelectorAll("img") 
  let imageObserver = new IntersectionObserver(
    function (entries) {
      entries.forEach(entry => {
        if (entry.isIntersecting) {
          let img = entry.target
          if (img.src.endsWith("assets/temp.webp")) {
            img.src = img.dataset.src
            imageObserver.unobserve(img)
          }
        }
      });
    })
  return imageObserver
}

/**
 *  Fetches and appends the required books to the webpage.
 * @param {"String"} q : Type of Categories of book to search for 
 */
async function fetchBooks(q) {
  // Get Books Promises
  let bookItemsFetch = await fetch(
    `https://books.googleapis.com/books/v1/volumes?q=${q}&maxResults=${MAX_RESULTS}&fields=items(id)&key=${apiKey()}`
    , {
      headers: {
        'Accept': 'application/json'
      }
    });
  // Check if fetch was successful
  if (bookItemsFetch.ok && bookItemsFetch.status === 200) {
    // Get Book Objects in json
    let bookItems = await bookItemsFetch.json()

    const volumes = []
    for (const book of bookItems.items) {
      // Get Each book volume by id
      let bookInfoFetch = await fetch(
        `https://books.googleapis.com/books/v1/volumes/${book.id}?fields=volumeInfo(title%2C%20imageLinks(thumbnail))&key=${apiKey()}`
        , {
          headers: {
            'Accept': 'application/json'
          }
        })

      // Check if fetch was successful
      if (bookInfoFetch.ok && bookInfoFetch.status === 200) {
        let bookInfo = await bookInfoFetch.json()

        volumes.push(bookInfo.volumeInfo)
      } else {
        document.querySelector("#error").style.visibility = "visible"
        console.error(bookInfoFetch.statusText, bookInfoFetch.text)
        return;
      }
    }

    // Create articles
    let articles = []
    volumes.forEach(volume => {
      articles.push(createArticle(volume))
    })

    //  Observe images
    let observer = getObserverLazyImages()
    articles.forEach(article => {
      let img = article.querySelector('img')
      observer.observe(img)
    })

    // Append articles
    let domFragment = new DocumentFragment()
    const containerId = getObjKey(CATEGORIES, q)
    domFragment.append(...articles)
    let container = document.querySelector(`#${containerId}`)
    container.querySelector(".card-container").appendChild(domFragment)

  } else {
    document.querySelector("#error").style.visibility = "visible"
    console.error(bookItemsFetch.statusText, bookItemsFetch.text)
    return;
  }
}


/**
 * This function add the srhink and grow animation to the image
 * 
 * @param {img} img : image to be assigned the annimation
 * @param {Number} WIDTH : width of the image
 */
function shrinkGrow(img, WIDTH, clickState) {
  function draw() {
    // Shrinks the image
    function shrink() {
      img.width -= 1
      img.height -= 1
      if (img.width === WIDTH) {
        clickState.isClickable = true
        cancelAnimationFrame(shrink)
        // cancelAnimationFrame(draw)
      } else {
        requestAnimationFrame(shrink)
      }
    }
    // Grows the image - default
    let newWidth = img.width
    if (newWidth <= WIDTH + 20 && newWidth >= WIDTH) {
      img.width += 1
      img.height += 1
    }

    if (newWidth === WIDTH + 20) {
      cancelAnimationFrame(draw)
      requestAnimationFrame(shrink)
    } else {
      requestAnimationFrame(draw)
    }
  }
  // Call Draw Method - if it can be clickable
  if (clickState.isClickable){
    draw()
    clickState.isClickable = false
  }
}

/**
 * This function creates an article for the volume/book provided
 * 
 * @param {*} volume : book gotten from api
 * @returns {article} : returns an article HTML element
 */
function createArticle(volume) {
  // Create elements
  const article = document.createElement("article")
  const figure = document.createElement("figure")
  const img = document.createElement("img")
  const secTitle = document.createElement("section")

  // Do Animation on click
  let state = { isClickable: true}
  article.addEventListener('click', (evt) => {
    if (evt.target.classList.contains('card-title')) {
      shrinkGrow(article.querySelector('img'), 
        article.querySelector('img').clientWidth,
        state)
    }
  })


  // Add Classes
  article.classList.add("card")
  figure.classList.add("card-thumbnail")
  secTitle.classList.add("card-title")
  // Add thumbnail
  let thumbnail = Object.keys(volume).includes("imageLinks")
  if (thumbnail) {
    img.setAttribute('data-src', volume.imageLinks.thumbnail)
    img.src = "assets/temp.webp"
  } else {
    img.setAttribute('data-src', "assets/notfound.webp")
    img.src = "assets/notfound.webp"
  }
  // Append to each other
  figure.appendChild(img)
  article.append(secTitle, figure)

  // Add title
  secTitle.textContent = volume.title

  return article
}

/**
 * This function gets the key of a Dictionary from its value
 * 
 * @param {Object} obj  : A Key/value object
 * @param {String} value : Value of the key wanted 
 * @returns {key} : Key of the value provided
 */
function getObjKey(obj, value) {
  return Object.keys(obj).find(key => obj[key] === value);
}